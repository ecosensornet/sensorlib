#include <Wire.h>

#include "sensor_aht15.h"

AHT15 sensor;

#define I2C_SDA 12
#define I2C_SCL 11

TwoWire i2c = TwoWire(0);


void setup() {
  Serial.begin(115200);
  i2c.setPins(I2C_SDA, I2C_SCL);
  i2c.begin();

  delay(500);
  while (!Serial) {
  }

  Serial.println ("aht15 ...");

  if (!sensor.begin(i2c)) {
    Serial.println("AHT15 sensor did not acknowledge! Freezing!");
    while (true);
  }
  Serial.println("AHT15 sensor did acknowledge.");
}

void loop() {
  sensor.sample();
  while (sensor.sample_not_ready()) {
    Serial.println("not ready");
    delay(40);
  }
  sensor.read_sample();
  
  Serial.println("temperature: " + String(sensor.temperature()) + " [°C]");
  Serial.println("humidity: " + String(sensor.humidity()) + " [%]");

  delay(1000);
}
