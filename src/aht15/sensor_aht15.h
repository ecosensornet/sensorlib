#ifndef SENSOR_AHT15_H
#define SENSOR_AHT15_H

#include "Arduino.h"
#include "Wire.h"

#define AHT15_DEBUG 0

#define AHT15_I2CADDR 0x38

#define AHT15_CMD_INIT 0xe1
#define AHT15_CMD_SAMPLE 0xac
#define AHT15_CMD_RESET 0xba
#define AHT15_CMD_STATUS 0x71


class AHT15 {
  public:
    AHT15(byte address = AHT15_I2CADDR);
    bool begin(TwoWire& wirePort = Wire);
    byte address();

    // current sensor status
    byte status();
    bool is_connected();

    // Launch and read sample.
    // It will return either 1 on success or 0 on failure. (Failure
    // can result from either a timed out I2C transmission, or an incorrect
    // checksum value).
    void sample(void);
    bool sample_not_ready(void);
    bool read_sample(void);

    // accessor
    // measured temperature in [°C].
    float temperature(void);

    // accessor
    // measured humidity in [%].
    float humidity(void);

  private:
    byte _address; // 7-bit I2C address
    TwoWire* _wire;
    
    // These keep track of the raw values read from the sensor:
    unsigned long _temperature_raw;  // ambient air temperature
    unsigned long _humidity_raw;  // ambient air relative humidity

};


#endif //  SENSOR_AHT15_H
