#include <Wire.h>

#include "sensor_aht21.h"

AHT21 sensor;

#define I2C_SDA 12
#define I2C_SCL 11

TwoWire i2c = TwoWire(0);


void setup() {
  Serial.begin(115200);
  i2c.setPins(I2C_SDA, I2C_SCL);
  i2c.begin();

  delay(500);
  while (!Serial) {
  }
  delay(5000);
  Serial.println ("aht21 ...");

  if (!sensor.begin(i2c)) {
    while (true) {
      Serial.println("Could not find a valid AHT21 sensor, check wiring!");
      delay(1000);
    }
  }
  Serial.println("AHT21 sensor did acknowledge.");
}

void loop() {
  Serial.println("loop");
  sensor.sample();
  while (sensor.sample_not_ready()) {
    Serial.println("sample not ready");
    delay(40);
  }
  sensor.read_sample();

  Serial.println("temperature: " + String(sensor.temperature()) + " [°C]");
  Serial.println("humidity: " + String(sensor.humidity()) + " [%]");

  delay(1000);
}
