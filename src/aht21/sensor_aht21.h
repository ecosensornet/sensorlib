#ifndef SENSOR_AHT21_H
#define SENSOR_AHT21_H

#include "Arduino.h"
#include "Wire.h"

#define AHT21_DEBUG 0

#define AHT21_I2CADDR 0x38


#define AHT21_CMD_INIT 0xe1
#define AHT21_CMD_SAMPLE 0xac
#define AHT21_CMD_SAMPLE_ARG0 0x33
#define AHT21_CMD_SAMPLE_ARG1 0x00
#define AHT21_CMD_RESET 0xba
#define AHT21_CMD_STATUS 0x71


class AHT21 {
  public:
    AHT21(byte address = AHT21_I2CADDR);
    bool begin(TwoWire& wirePort = Wire);
    byte address();

    // current sensor status
    byte status();
    bool is_connected();

    // Launch and read sample.
    void sample(void);
    bool sample_not_ready(void);
    bool read_sample(void);

    // accessor
    // measured temperature in [°C].
    float temperature(void);

    // accessor
    // measured humidity in [%].
    float humidity(void);

  private:
    byte _address; // 7-bit I2C address
    TwoWire* _wire;

    // These keep track of the raw values read from the sensor:
    unsigned long _temperature_raw;  // ambient air temperature
    unsigned long _humidity_raw;  // ambient air relative humidity

};


#endif //  SENSOR_AHT21_H
