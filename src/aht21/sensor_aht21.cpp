#include "sensor_aht21.h"


AHT21::AHT21(byte address) : _address(address) {
}


bool AHT21::begin(TwoWire& wirePort) {
  _wire = &wirePort;

  return is_connected();
}


// I2C address of device
byte AHT21::address() {
  return _address;
}


// check connection to device
bool AHT21::is_connected() {
  byte stat = status();
#if (AHT21_DEBUG == 1)
  Serial.println("status " + String(stat) + ", &0x18: " + String(stat & 0x18) + ", cmp: " + String((byte)(stat & 0x18) == 0x18));
#endif

  return (byte)(stat & 0x18) == 0x18;
}


void AHT21::sample(void) {
  _wire->beginTransmission(_address);
  _wire->write(AHT21_CMD_SAMPLE);
  _wire->write(AHT21_CMD_SAMPLE_ARG0);
  _wire->write(AHT21_CMD_SAMPLE_ARG1);
  _wire->endTransmission();
}


bool AHT21::sample_not_ready(void) {
  return (byte)(status() & 0x80) == 0x80;
}


bool AHT21::read_sample(void) {
  unsigned long buf[6];
  _wire->requestFrom(_address, (uint8_t)6);
  for (unsigned char i = 0; _wire->available() > 0; i++)  {
    buf[i] = _wire->read();
#if (AHT21_DEBUG == 1)
    Serial.println("read: " + String(buf[i]) + " bla");
#endif
  }

  _humidity_raw = ((buf[1] << 16) | (buf[2] << 8) | buf[3]) >> 4;
  _temperature_raw = ((buf[3] & 0x0f) << 16) | (buf[4] << 8) | buf[5];
#if (AHT21_DEBUG == 1)
  Serial.println("raw: " + String(_temperature_raw) + ", rh: " + String(_humidity_raw));
#endif

  return true;
}


float AHT21::temperature(void) {
  return ((float)_temperature_raw * 200 / 1048576) - 50;
}


float AHT21::humidity(void) {
  if (_humidity_raw == 0) {
    return 0;                       // Some unrealistic value
  }
  return (float)_humidity_raw * 100 / 1048576;
}


byte AHT21::status() {
  _wire->beginTransmission(_address);
  _wire->write(AHT21_CMD_STATUS);
  _wire->endTransmission();

  _wire->requestFrom(_address, (uint8_t)1);
  return _wire->read();
}
