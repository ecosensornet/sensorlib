#include "sensor_max6675.h"

int thermoSO = 9;
int thermoCS = 7;
int thermoCLK = 12;

MAX6675 th = MAX6675(thermoCLK, thermoCS, thermoSO);

void setup() {
  Serial.begin(115200);
  delay(300);

  pinMode(11, OUTPUT);
  digitalWrite(11, HIGH);

  if (!th.begin()) {
    while (1) {
      Serial.println("sensor not responding");
      delay(1000);
    }
  }

  Serial.println("setup");

}

void loop() {
  th.sample();
  while (th.sample_not_ready()) {
    delay(10);
  }
  th.read_sample();
  
  Serial.println("Temperature: " + String(th.temperature(), 2) + " [°C]");

  delay(1000);
}
