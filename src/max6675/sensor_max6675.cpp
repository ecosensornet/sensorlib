#include "sensor_max6675.h"

MAX6675::MAX6675(uint8_t sclk_pin, uint8_t cs_pin, uint8_t miso_pin) {
  _sclk = sclk_pin;
  _cs = cs_pin;
  _miso = miso_pin;
}

bool MAX6675::begin(void) {
  // define pin modes
  pinMode(_cs, OUTPUT);
  pinMode(_sclk, OUTPUT);
  pinMode(_miso, INPUT);

  digitalWrite(_cs, HIGH);

  return true;
}


void MAX6675::sample(void) {

}


bool MAX6675::sample_not_ready(void) {
  return false;
}


bool MAX6675::read_sample(void) {
  digitalWrite(_cs, LOW);
  delayMicroseconds(10);

  _temperature_raw = spiread();
  _temperature_raw <<= 8;
  _temperature_raw |= spiread();

  digitalWrite(_cs, HIGH);

  return true;
}

float MAX6675::temperature(void) {
  if (_temperature_raw & 0x4) {
    // uh oh, no thermocouple attached!
    return NAN;
  }

  return (_temperature_raw >> 3) * 0.25;
}


byte MAX6675::spiread(void) {
  int i;
  byte d = 0;

  for (i = 7; i >= 0; i--) {
    digitalWrite(_sclk, LOW);
    delayMicroseconds(10);
    if (digitalRead(_miso)) {
      // set the bit to 0 no matter what
      d |= (1 << i);
    }

    digitalWrite(_sclk, HIGH);
    delayMicroseconds(10);
  }

  return d;
}
