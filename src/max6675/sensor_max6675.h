#ifndef SENSOR_MAX6675_H
#define SENSOR_MAX6675_H

#include "Arduino.h"

class MAX6675 {
  public:
    MAX6675(uint8_t sclk_pin, uint8_t cs_pin, uint8_t miso_pin);
    bool begin();

    void sample(void);
    bool sample_not_ready(void);
    bool read_sample(void);

    float temperature(void);

  private:
    uint8_t _sclk;
    uint8_t _miso;
    uint8_t _cs;

    uint16_t _temperature_raw;
    
    uint8_t spiread(void);
};


#endif //  SENSOR_MAX6675_H
