#include <component_mcp3421.h>
#include <Wire.h>


MCP3421 light = MCP3421();

#define I2C_SDA 12
#define I2C_SCL 11

TwoWire i2c = TwoWire(0);



void setup(void) {
  Serial.begin(115200);
  i2c.setPins(I2C_SDA, I2C_SCL);
  i2c.begin();
  delay(300);

  light.begin(i2c);
  delay(1); // MC3421 needs 300us to settle, wait 1ms

  // Check device present
  if (!light.is_connected()) {
    Serial.print("No device found at address ");
    Serial.println(light.address(), HEX);
    while (1)
      ;
  }

  light.configure(MCP3421::one_shot, MCP3421::res_18, MCP3421::gain_8);
  light.trigger();
}

void loop(void) {
  int32_t raw_val;
  float val;

  if (light.is_ready()) {
    raw_val = light.raw_value();
    val = light.value();
    Serial.printf("\nLight: 0x%08lX (%ld) -> %f [mV]\n", raw_val, raw_val, val);
    light.trigger();
  }
  else {
    Serial.printf(".");
  }
  delay(1000);
}
