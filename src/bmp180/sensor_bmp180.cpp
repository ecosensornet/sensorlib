#include "sensor_bmp180.h"

BMP180::BMP180(byte address, uint8_t mode) {
  _address = address;
  if (mode > BMP180_ULTRAHIGHRES)
    mode = BMP180_ULTRAHIGHRES;
  oversampling = mode;
}


boolean BMP180::begin(TwoWire &wirePort) {
  _wire = &wirePort;

  if (!is_connected()) {
    return false;
  }

  /* read calibration data */
  ac1 = read16(BMP180_CAL_AC1);
  ac2 = read16(BMP180_CAL_AC2);
  ac3 = read16(BMP180_CAL_AC3);
  ac4 = read16(BMP180_CAL_AC4);
  ac5 = read16(BMP180_CAL_AC5);
  ac6 = read16(BMP180_CAL_AC6);

  b1 = read16(BMP180_CAL_B1);
  b2 = read16(BMP180_CAL_B2);

  mb = read16(BMP180_CAL_MB);
  mc = read16(BMP180_CAL_MC);
  md = read16(BMP180_CAL_MD);
#if (BMP180_DEBUG == 1)
  Serial.print("ac1 = "); Serial.println(ac1, DEC);
  Serial.print("ac2 = "); Serial.println(ac2, DEC);
  Serial.print("ac3 = "); Serial.println(ac3, DEC);
  Serial.print("ac4 = "); Serial.println(ac4, DEC);
  Serial.print("ac5 = "); Serial.println(ac5, DEC);
  Serial.print("ac6 = "); Serial.println(ac6, DEC);

  Serial.print("b1 = "); Serial.println(b1, DEC);
  Serial.print("b2 = "); Serial.println(b2, DEC);

  Serial.print("mb = "); Serial.println(mb, DEC);
  Serial.print("mc = "); Serial.println(mc, DEC);
  Serial.print("md = "); Serial.println(md, DEC);
#endif

  return true;
}

// I2C address of device
byte BMP180::address() {
  return _address;
}


// check connection to device
bool BMP180::is_connected() {
#if BMP180_DEBUG == 1
  Serial.println("is_connected");
#endif
  return read8(0xD0) == 0x55;
}


void BMP180::sample(void) {
  write8(BMP180_CONTROL, BMP180_CMD_SAMPLE_TEMP);
  delay(5);
  _temperature_raw = read16(BMP180_CMD_READ_SAMPLE);

  write8(BMP180_CONTROL, BMP180_CMD_SAMPLE_PRESSURE + (oversampling << 6));
}


bool BMP180::sample_not_ready(void) {
  byte status= read8(BMP180_CONTROL);
  return (status >> 5) & 0b1 == 1;
}


bool BMP180::read_sample(void) {
  // _temperature_raw already measured
  _pressure_raw = read16(BMP180_CMD_READ_SAMPLE);

  _pressure_raw <<= 8;
  _pressure_raw |= read8(BMP180_CMD_READ_SAMPLE + 2);
  _pressure_raw >>= (8 - oversampling);

  return true;
}


int32_t BMP180::computeB5(int32_t UT) {
  int32_t X1 = (UT - (int32_t)ac6) * ((int32_t)ac5) >> 15;
  int32_t X2 = ((int32_t)mc << 11) / (X1 + (int32_t)md);
  return X1 + X2;
}


int32_t BMP180::pressure(void) {
  int32_t UT, UP, B3, B5, B6, X1, X2, X3, p;
  uint32_t B4, B7;

  UT = _temperature_raw;
  UP = _pressure_raw;

#if BMP180_DEBUG == 2
  // use datasheet numbers!
  UT = 27898;
  UP = 23843;
  ac6 = 23153;
  ac5 = 32757;
  mc = -8711;
  md = 2868;
  b1 = 6190;
  b2 = 4;
  ac3 = -14383;
  ac2 = -72;
  ac1 = 408;
  ac4 = 32741;
  oversampling = 0;
#endif

  B5 = computeB5(UT);

#if BMP180_DEBUG == 1
  Serial.print("UT = "); Serial.println(UT);
  Serial.print("UP = "); Serial.println(UP);
  Serial.print("B5 = "); Serial.println(B5);
#endif

  // do pressure calcs
  B6 = B5 - 4000;
  X1 = ((int32_t)b2 * ( (B6 * B6) >> 12 )) >> 11;
  X2 = ((int32_t)ac2 * B6) >> 11;
  X3 = X1 + X2;
  B3 = ((((int32_t)ac1 * 4 + X3) << oversampling) + 2) / 4;

#if BMP180_DEBUG == 1
  Serial.print("B6 = "); Serial.println(B6);
  Serial.print("X1 = "); Serial.println(X1);
  Serial.print("X2 = "); Serial.println(X2);
  Serial.print("B3 = "); Serial.println(B3);
#endif

  X1 = ((int32_t)ac3 * B6) >> 13;
  X2 = ((int32_t)b1 * ((B6 * B6) >> 12)) >> 16;
  X3 = ((X1 + X2) + 2) >> 2;
  B4 = ((uint32_t)ac4 * (uint32_t)(X3 + 32768)) >> 15;
  B7 = ((uint32_t)UP - B3) * (uint32_t)( 50000UL >> oversampling );

#if BMP180_DEBUG == 1
  Serial.print("X1 = "); Serial.println(X1);
  Serial.print("X2 = "); Serial.println(X2);
  Serial.print("B4 = "); Serial.println(B4);
  Serial.print("B7 = "); Serial.println(B7);
#endif

  if (B7 < 0x80000000) {
    p = (B7 * 2) / B4;
  } else {
    p = (B7 / B4) * 2;
  }
  X1 = (p >> 8) * (p >> 8);
  X1 = (X1 * 3038) >> 16;
  X2 = (-7357 * p) >> 16;

#if BMP180_DEBUG == 1
  Serial.print("p = "); Serial.println(p);
  Serial.print("X1 = "); Serial.println(X1);
  Serial.print("X2 = "); Serial.println(X2);
#endif

  p = p + ((X1 + X2 + (int32_t)3791) >> 4);
#if BMP180_DEBUG == 1
  Serial.print("p = "); Serial.println(p);
#endif
  return p;
}

int32_t BMP180::sea_level_pressure(float altitude_meters) {
  return (int32_t)((float)pressure() / pow(1.0 - altitude_meters / 44330, 5.255));
}

float BMP180::temperature(void) {
  int32_t UT, B5;     // following ds convention
  float temp;

  UT = _temperature_raw;

#if BMP180_DEBUG == 1
  // use datasheet numbers!
  UT = 27898;
  ac6 = 23153;
  ac5 = 32757;
  mc = -8711;
  md = 2868;
#endif

  B5 = computeB5(UT);
  temp = (B5 + 8) >> 4;
  temp /= 10;

  return temp;
}

float BMP180::altitude(float sea_level_pressure) {
  return 44330 * (1.0 - pow(pressure() / sea_level_pressure, 0.1903));
}


/*********************************************************************/

uint8_t BMP180::read8(uint8_t a) {
  uint8_t ret;

  _wire->beginTransmission(_address);
  _wire->write(a); // sends register address to read from
  _wire->endTransmission();

  _wire->requestFrom(_address, (uint8_t)1);
  ret = _wire->read();
  _wire->endTransmission();
#if BMP180_DEBUG == 1
  Serial.print("read8: ");
  Serial.println(ret);
#endif

  return ret;
}

uint16_t BMP180::read16(uint8_t a) {
  uint16_t ret;

  _wire->beginTransmission(_address);
  _wire->write(a); // sends register address to read from
  _wire->endTransmission();

  _wire->requestFrom(_address, (uint8_t)2);
  ret = _wire->read();
  ret <<= 8;
  ret |= _wire->read();
  _wire->endTransmission();

  return ret;
}

void BMP180::write8(uint8_t a, uint8_t d) {
  _wire->beginTransmission(_address);
  _wire->write(a);
  _wire->write(d);
  _wire->endTransmission();
}
