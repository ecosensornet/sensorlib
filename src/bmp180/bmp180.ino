#include <Wire.h>

#include "sensor_bmp180.h"

BMP180 sensor;

#define I2C_SDA 12
#define I2C_SCL 11

TwoWire i2c = TwoWire(0);


void setup() {
  Serial.begin(115200);

  i2c.setPins(I2C_SDA, I2C_SCL);
  i2c.begin();

  Serial.println("setup begin");

  if (!sensor.begin(i2c)) {
    while (true) {
      Serial.println("Could not find a valid BMP180 sensor, check wiring!");
      delay(1000);
    }
  }
}

void loop() {
  Serial.println("loop");

  sensor.sample();
  while (sensor.sample_not_ready()) {
    Serial.println("sample not ready");
    delay(40);
  }
  sensor.read_sample();

  Serial.println("Temperature: " + String(sensor.temperature(), 2) + " [°C]");
  Serial.println("Pressure: " + String(sensor.pressure()) + " [Pa]");

  // Calculate altitude assuming 'standard' barometric
  // pressure of 1013.25 millibar = 101325 Pascal
  Serial.println("Altitude (calculated): " + String(sensor.altitude(), 2) + " [m]");

  Serial.println("Pressure at sea level (calculated): " + String(sensor.sea_level_pressure()) + " [Pa]");

  delay(1000);
}
