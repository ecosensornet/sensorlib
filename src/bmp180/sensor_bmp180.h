#ifndef SENSOR_BMP180_H
#define SENSOR_BMP180_H

#include "Arduino.h"
#include "Wire.h"

#define BMP180_DEBUG 0

#define BMP180_I2CADDR 0x77

#define BMP180_ULTRALOWPOWER 0
#define BMP180_STANDARD      1
#define BMP180_HIGHRES       2
#define BMP180_ULTRAHIGHRES  3
#define BMP180_CAL_AC1           0xAA  // R   Calibration data (16 bits)
#define BMP180_CAL_AC2           0xAC  // R   Calibration data (16 bits)
#define BMP180_CAL_AC3           0xAE  // R   Calibration data (16 bits)    
#define BMP180_CAL_AC4           0xB0  // R   Calibration data (16 bits)
#define BMP180_CAL_AC5           0xB2  // R   Calibration data (16 bits)
#define BMP180_CAL_AC6           0xB4  // R   Calibration data (16 bits)
#define BMP180_CAL_B1            0xB6  // R   Calibration data (16 bits)
#define BMP180_CAL_B2            0xB8  // R   Calibration data (16 bits)
#define BMP180_CAL_MB            0xBA  // R   Calibration data (16 bits)
#define BMP180_CAL_MC            0xBC  // R   Calibration data (16 bits)
#define BMP180_CAL_MD            0xBE  // R   Calibration data (16 bits)

#define BMP180_CONTROL           0xF4
#define BMP180_CMD_READ_SAMPLE          0xF6
#define BMP180_CMD_SAMPLE_TEMP      0x2E
#define BMP180_CMD_SAMPLE_PRESSURE  0x34


class BMP180 {
  public:
    BMP180(byte address = BMP180_I2CADDR, uint8_t mode = BMP180_ULTRAHIGHRES);
    bool begin(TwoWire& wirePort = Wire);
    byte address();

    // current sensor status
    byte status();
    bool is_connected();

    // pulls the latest values from the sensor.
    void sample(void);
    bool sample_not_ready(void);
    bool read_sample(void);

    // accessor
    // measured temperature in [°C].
    float temperature(void);

    // accessor
    // measured temperature in [°C].
    int32_t pressure(void);
    int32_t sea_level_pressure(float altitude_meters = 0);
    float altitude(float sea_level_pressure = 101325); // std atmosphere

  private:
    uint8_t _address; // 7-bit I2C address
    TwoWire* _wire;

    uint8_t oversampling;

    int16_t ac1, ac2, ac3, b1, b2, mb, mc, md;
    uint16_t ac4, ac5, ac6;

    // These keep track of the raw values read from the sensor:
    uint16_t _temperature_raw;  // measured air temperature
    uint32_t _pressure_raw;  // measured pressure

    uint16_t readRawTemperature(void);
    uint32_t readRawPressure(void);
    int32_t computeB5(int32_t UT);
    uint8_t read8(uint8_t addr);
    uint16_t read16(uint8_t addr);
    void write8(uint8_t addr, uint8_t data);

};


#endif //  SENSOR_BMP180_H
