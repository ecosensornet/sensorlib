#include <Wire.h>

#include "sensor_mlx90614.h"

MLX90614 sensor;

#define I2C_SDA 12
#define I2C_SCL 11

TwoWire i2c = TwoWire(0);

void setup()
{
  Serial.begin(115200);
  i2c.setPins(I2C_SDA, I2C_SCL);
  i2c.begin();

  while (!Serial) {
    delay(30);
  }

  Serial.println ("Thermal ...");

  if (!sensor.begin(i2c)) { // Initialize thermal IR sensor
    Serial.println("Qwiic IR thermometer did not acknowledge! Freezing!");
    while (true);
  }
  Serial.println("Qwiic IR Thermometer did acknowledge.");
}

void loop() {
  Serial.println("loop");

  sensor.sample();
  while (sensor.sample_not_ready()) {
    Serial.println("sample not ready");
    delay(40);
  }
  sensor.read_sample();

  Serial.println("Ambient: " + String(sensor.temp_ambient(), 2)
                 + " Object: " + String(sensor.temp_object(), 2) + " [K]");

  delay(1000);
}
