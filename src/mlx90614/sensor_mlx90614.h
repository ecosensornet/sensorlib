#ifndef SENSOR_MLX90614_H
#define SENSOR_MLX90614_H

#include <Arduino.h>
#include <Wire.h>

//////////////////////////////////
// MLX90614 Default I2C Address //
//////////////////////////////////
#define MLX90614_DEFAULT_ADDRESS 0x5A

/* registers */
#define MLX90614_REGISTER_TA      0x06
#define MLX90614_REGISTER_TOBJ1	  0x07
#define MLX90614_REGISTER_TOBJ2	  0x08
#define MLX90614_REGISTER_TOMAX   0x20
#define MLX90614_REGISTER_TOMIN   0x21
#define MLX90614_REGISTER_PWMCTRL 0x22
#define MLX90614_REGISTER_TARANGE 0x23
#define MLX90614_REGISTER_KE      0x24
#define MLX90614_REGISTER_CONFIG  0x25
#define MLX90614_REGISTER_ADDRESS 0x2E
#define MLX90614_REGISTER_ID0     0x3C
#define MLX90614_REGISTER_ID1     0x3D
#define MLX90614_REGISTER_ID2     0x3E
#define MLX90614_REGISTER_ID3     0x3F
#define MLX90614_REGISTER_SLEEP   0xFF // Not really a register, but close enough

#define I2C_READ_TIMEOUT 1000


class MLX90614 {
  public:
    MLX90614(uint8_t address = MLX90614_DEFAULT_ADDRESS);
    bool begin(TwoWire &wirePort = Wire);
    uint8_t address();

    // current sensor status
    bool is_connected();

    // pulls the latest values from the sensor.
    void sample(void);
    bool sample_not_ready(void);
    bool read_sample(void);

    // accessor
    // measured temperature in [°K].
    float temp_object(void);
    float temp_ambient(void);

    // Reads the MLX90614's emissivity setting. It will
    // return a value between 0.1 and 1.0.
    // a value of 0 means read failed.
    float emissivity();
    uint8_t set_emissivity(float emis);

    // Returns the MLX90614's configured 7-bit I2C bus address.
    // A value between 0x01 and 0x7F should be returned.
    uint8_t i2c_address();
    // The new address won't take effect until the device is reset.
    bool set_i2c_address(uint8_t address);

  private:
    uint8_t _address; // MLX90614's 7-bit I2C address
    TwoWire* _wire;

    // These keep track of the raw temperature values read from the sensor:
    int16_t _ambient_raw;  // ambient air temperature
    int16_t _object_raw;  // measured object sensor

    bool readAmbient(void);
    bool readObject(void);

    // calcTemperature converts a raw ADC temperature reading to the
    // set unit.
    float convert_temp(int16_t temp_raw);

    // Abstract function to write 16-bits to an address in the MLX90614's
    // EEPROM
    bool write_eeprom(byte reg, int16_t data);

    // Abstract functions to read and write 16-bit values from a RAM
    // or EEPROM address in the MLX90614
    bool i2c_read_word(byte reg, int16_t* dest);
    uint8_t i2c_write_word(byte reg, int16_t data);

    // crc8 returns a calculated crc value given an initial value and
    // input data.
    // It's configured to calculate the CRC using a x^8+x^2+x^1+1 poly
    uint8_t crc8 (uint8_t inCrc, uint8_t inData);
};

#endif  // SENSOR_MLX90614_H
