#include "sensor_mlx90614.h"

MLX90614::MLX90614(uint8_t address) {
  _address = address;
  _ambient_raw = 0;
  _object_raw = 0;
}

bool MLX90614::begin(TwoWire& wirePort) {
  _wire = &wirePort;

  return true;
}

// I2C address of device
uint8_t MLX90614::address() {
  return _address;
}

bool MLX90614::is_connected() {
  _wire->beginTransmission(_address);
  if (_wire->endTransmission() == 0) {
    return true;
  }
  return false;
}

void MLX90614::sample(void) {}  // useful once power down is implemented
bool MLX90614::sample_not_ready(void) {
  return false;
}

bool MLX90614::read_sample(void) {
  int16_t raw;

  // read ambient temp from sensor
  if (!i2c_read_word(MLX90614_REGISTER_TA, &raw)) {
    return false;
  }
  _ambient_raw = raw;

  // read object temp from sensor
  if (!i2c_read_word(MLX90614_REGISTER_TOBJ1, &raw)) {
    return false;
  }
  if (raw & 0x8000) {// If there was a flag error
    return false; // Return fail
  }
  _object_raw = raw;

  return true;
}

float MLX90614::temp_ambient(void) {
  return convert_temp(_ambient_raw);
}


float MLX90614::temp_object(void) {
  return convert_temp(_object_raw);
}


float MLX90614::emissivity() {
  int16_t ke;
  if (i2c_read_word(MLX90614_REGISTER_KE, &ke)) {
    // If we successfully read from the ke register
    // calculate the emissivity between 0.1 and 1.0:
    return (((float)((uint16_t)ke)) / 65535.0);
  }
  return false;
}

uint8_t MLX90614::set_emissivity(float emis) {
  // Make sure emissivity is between 0.1 and 1.0
  if ((emis > 1.0) || (emis < 0.1)) {
    return false;
  }
  // Calculate the raw 16-bit value:
  uint16_t ke = uint16_t(65535.0 * emis);
  ke = constrain(ke, 0x2000, 0xFFFF);

  // Write that value to the ke register
  return write_eeprom(MLX90614_REGISTER_KE, (int16_t)ke);
}


uint8_t MLX90614::i2c_address() {
  int16_t tempAdd;
  // Read from the 7-bit I2C address EEPROM storage address:
  if (i2c_read_word(MLX90614_REGISTER_ADDRESS, &tempAdd))	{
    // If read succeeded, return the address:
    return (uint8_t) tempAdd;
  }
  return false; // Else return fail
}

bool MLX90614::set_i2c_address(uint8_t address) {
  int16_t tempAdd;
  // Make sure the address is within the proper range:
  if ((address >= 0x80) || (address == 0x00)) {
    return false; // Return fail if out of range
  }
  // Read from the I2C address address first:
  if (i2c_read_word(MLX90614_REGISTER_ADDRESS, &tempAdd)) {
    tempAdd &= 0xFF00; // Mask out the address (MSB is junk?)
    tempAdd |= address; // Add the new address

    // Write the new address back to EEPROM:
    return write_eeprom(MLX90614_REGISTER_ADDRESS, tempAdd);
  }
  return true;
}


float MLX90614::convert_temp(int16_t temp_raw) {
  return float(temp_raw) * 0.02;
}

bool MLX90614::i2c_read_word(byte reg, int16_t* dest) {
  _wire->beginTransmission(_address);
  _wire->write(reg);

  _wire->endTransmission(false); // Send restart
  _wire->requestFrom(_address, (uint8_t) 3);

  uint8_t lsb = _wire->read();
  uint8_t msb = _wire->read();
  uint8_t pec = _wire->read();

  uint8_t crc = crc8(0, (_address << 1));
  crc = crc8(crc, reg);
  crc = crc8(crc, (_address << 1) + 1);
  crc = crc8(crc, lsb);
  crc = crc8(crc, msb);

  if (crc == pec)	{
    *dest = (msb << 8) | lsb;
    return true;
  }
  else {
    return false;
  }
}

bool MLX90614::write_eeprom(byte reg, int16_t data) {
  // Clear out EEPROM first:
  if (i2c_write_word(reg, 0) != 0) {
    return false;
  }
  delay(5); // Delay to erase

  uint8_t i2cRet = i2c_write_word(reg, data);
  delay(5); // Delay to write

  if (i2cRet == 0)
    return true;
  else
    return false;
}

uint8_t MLX90614::i2c_write_word(byte reg, int16_t data) {
  uint8_t crc;
  uint8_t lsb = data & 0x00FF;
  uint8_t msb = (data >> 8);

  crc = crc8(0, (_address << 1));
  crc = crc8(crc, reg);
  crc = crc8(crc, lsb);
  crc = crc8(crc, msb);

  _wire->beginTransmission(_address);
  _wire->write(reg);
  _wire->write(lsb);
  _wire->write(msb);
  _wire->write(crc);
  return _wire->endTransmission(true);
}

uint8_t MLX90614::crc8 (uint8_t inCrc, uint8_t inData) {
  uint8_t i;
  uint8_t data;
  data = inCrc ^ inData;
  for ( i = 0; i < 8; i++ ) {
    if (( data & 0x80 ) != 0 ) {
      data <<= 1;
      data ^= 0x07;
    }
    else {
      data <<= 1;
    }
  }
  return data;
}
