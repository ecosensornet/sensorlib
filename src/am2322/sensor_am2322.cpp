#include "sensor_am2322.h"


AM2322::AM2322() {
}


void AM2322::begin(byte address, TwoWire &wirePort) {
  _address = address;
  _wire = &wirePort;
}


// I2C address of device
byte AM2322::address() {
  return _address;
}


// check connection to device
bool AM2322::is_connected() {
#if (AM2322_DEBUG == 1)
  Serial.println("is_connected");
#endif
  _wire->beginTransmission(_address);
  _wire->write(0x03);
  _wire->write(0x0F);
  _wire->write(1);
  int rv = _wire->endTransmission();
  
  if (rv < 0) return false;  // fail
  if (rv != AM2322_OK) return false;  // fail

  return true;
}


void AM2322::sample(void) {
  if (millis() - _last_read > 60000) {  // sensor has been sleeping for long, force dummy read
#if (AM2322_DEBUG == 1)
    Serial.println("reawaken");
#endif
    read_sample();
  }
}


bool AM2322::sample_not_ready(void) {
  return millis() - _last_read <= _read_delay;
}


bool AM2322::read_sample(void) {
  _last_read = millis();

  int rv = _read_register(0x00, 4);
  if (rv < 0) return false;  // fail
  if (rv != AM2322_OK) return false;  // fail

  _humidity_raw = _bits[2] * 256 + _bits[3];
  int16_t t = ((_bits[4] & 0x7F) * 256 + _bits[5]);
  if (t == 0)  {
    _temperature_raw = 0;     // prevent -0.0;
  }
  else  {
    if ((_bits[4] & 0x80) == 0x80 )    {
      _temperature_raw = -t;
    }
    else {
      _temperature_raw = t;
    }
  }

#if (AM2322_DEBUG == 1)
  Serial.println("raw: " + String(_temperature_raw) + ", rh: " + String(_humidity_raw));
#endif

  return true;
}


float AM2322::temperature(void) {
  return (float)_temperature_raw * 0.1;
}


float AM2322::humidity(void) {
  return (float)_humidity_raw * 0.1;
}


byte AM2322::status() {
  int rv = _read_register(0x0F, 1);
  if (rv < 0) return 0;

  return _bits[2];

}

int AM2322::_read_register(uint8_t reg, uint8_t cnt) {
  // HANDLE PENDING IRQ
  yield();

  // WAKE UP the sensor
  if (! wake_up() ) return AM2322_ERROR_CONNECT;

  // request the data
  _wire->beginTransmission(_address);
  _wire->write(0x03);
  _wire->write(reg);
  _wire->write(cnt);
  int rv = _wire->endTransmission();
  if (rv < 0) return rv;

  // request 4 extra, 2 for cmd + 2 for CRC
  rv = _get_data(cnt + 4);
  return rv;

}
int AM2322::_write_register(uint8_t reg, uint8_t cnt, int16_t value) {
  if (! wake_up() ) return AM2322_ERROR_CONNECT;

  // prepare data to send
  _bits[0] = 0x10;
  _bits[1] = reg;
  _bits[2] = cnt;

  if (cnt == 2) {
    _bits[4] = value & 0xFF;
    _bits[3] = (value >> 8) & 0xFF;
  }
  else {
    _bits[3] = value & 0xFF;
  }

  // send data
  uint8_t length = cnt + 3;  // 3 = cmd, startReg, #bytes
  _wire->beginTransmission(_address);
  for (int i = 0; i < length; i++) {
    _wire->write(_bits[i]);
  }
  // send the CRC
  uint16_t crc = _crc16(_bits, length);
  _wire->write(crc & 0xFF);
  _wire->write(crc >> 8);

  int rv = _wire->endTransmission();
  if (rv < 0) return rv;

  // wait for the answer
  rv = _get_data(length);
  return rv;
}
int AM2322::_get_data(uint8_t length) {
  int bytes = _wire->requestFrom(_address, length);
  if (bytes == 0) return AM2322_ERROR_CONNECT;

  for (int i = 0; i < bytes; i++) {
    _bits[i] = _wire->read();
  }

  // ANALYZE ERRORS
  //   will not detect if we requested 1 byte as that will
  //   return 5 bytes as requested. E.g. getStatus()
  //   design a fix if it becomes a problem.
  if (bytes != length) {
    switch (_bits[3]) {
      case 0x80: return AM2322_ERROR_FUNCTION;
      case 0x81: return AM2322_ERROR_ADDRESS;
      case 0x82: return AM2322_ERROR_REGISTER;
      case 0x83: return AM2322_ERROR_CRC_1;  // previous write had a wrong CRC
      case 0x84: return AM2322_ERROR_WRITE_DISABLED;
      default:   return AM2322_ERROR_UNKNOWN;
    }
  }

  // CRC is LOW Byte first
  uint16_t crc = _bits[bytes - 1] * 256 + _bits[bytes - 2];
  if (_crc16(&_bits[0], bytes - 2) != crc) {
    return AM2322_ERROR_CRC_2;  // read itself has wrong CRC
  }

  return AM2322_OK;

}

uint16_t AM2322::_crc16(uint8_t *ptr, uint8_t len) {
  uint16_t crc = 0xFFFF;
  while (len--) {
    crc ^= *ptr++;
    for (uint8_t i = 0; i < 8; i++) {
      if (crc & 0x01) {
        crc >>= 1;
        crc ^= 0xA001;
      }
      else {
        crc >>= 1;
      }
    }
  }
  return crc;

}
