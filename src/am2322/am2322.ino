#include <Wire.h>
#include "sensor_am2322.h"

#define LED_BUILTIN 15

#define I2C_SDA 18
#define I2C_SCL 33

TwoWire i2c = TwoWire(0);
AM2322 sensor;


void setup() {
  Serial.begin(115200);
  i2c.setPins(I2C_SDA, I2C_SCL);
  i2c.begin();
  pinMode(LED_BUILTIN, OUTPUT);
  for (uint8_t i = 0; i < 10; i++) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(300);
    digitalWrite(LED_BUILTIN, LOW);
    delay(200);
  }

  while (!Serial) {
    delay(10);
  }
  Serial.println("am2322 begin");
  sensor.begin(AM2322_I2CADDR, i2c);
  sensor.wake_up(); // dummy call to wake up sensor
  
  Serial.println("am2322 is connected?");
  while (! sensor.is_connected() ) {
    Serial.println("Sensor not found");
    delay(500);
  }

  Serial.println("Type,\tStatus,\tHumidity (%),\tTemperature (C)");
  Serial.println ("am2322 ...");
}

void loop() {
  Serial.println("loop");
  sensor.sample();
  while (sensor.sample_not_ready()) {
    delay(40);
  }

  if (sensor.read_sample()) {
    Serial.print(sensor.humidity(), 1);
    Serial.print(",\t");
    Serial.println(sensor.temperature(), 1);

  }

  delay(1000);
}
