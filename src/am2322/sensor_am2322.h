#ifndef SENSOR_AM2322_H
#define SENSOR_AM2322_H

#include "Arduino.h"
#include "Wire.h"

#define AM2322_DEBUG 0

#define AM2322_I2CADDR 0x5c

#define AM2322_OK                        0
#define AM2322_ERROR_UNKNOWN            -10
#define AM2322_ERROR_CONNECT            -11
#define AM2322_ERROR_FUNCTION           -12
#define AM2322_ERROR_ADDRESS            -13
#define AM2322_ERROR_REGISTER           -14
#define AM2322_ERROR_CRC_1              -15
#define AM2322_ERROR_CRC_2              -16
#define AM2322_ERROR_WRITE_DISABLED     -17
#define AM2322_ERROR_WRITE_COUNT        -18
#define AM2322_MISSING_BYTES            -19
#define AM2322_READ_TOO_FAST            -20

class AM2322 {
  public:
    AM2322();
    void begin(byte address = AM2322_I2CADDR, TwoWire& wirePort = Wire);
    byte address();

    // current sensor status
    byte status();
    bool is_connected();

    bool wake_up() {
      return is_connected();
    };

    // pulls the latest ambient and object temperatures.
    void sample(void);
    bool sample_not_ready(void);
    bool read_sample(void);

    // accessor
    // measured temperature in [°C].
    float temperature(void);

    // accessor
    // measured humidity in [%].
    float humidity(void);

  private:
    byte _address; // 7-bit I2C address
    TwoWire* _wire;

    uint8_t _bits[8]; // buffer to hold raw data
    uint32_t _last_read = 0;  // last time reading was made
    uint32_t _read_delay = 2000;  // [ms]

    // These keep track of the raw values read from the sensor:
    long _temperature_raw;  // ambient air temperature
    unsigned long _humidity_raw;  // ambient air relative humidity

    int _read_register(uint8_t reg, uint8_t cnt);
    int _write_register(uint8_t reg, uint8_t cnt, int16_t value);
    int _get_data(uint8_t length);

    uint16_t _crc16(uint8_t *ptr, uint8_t len);

};


#endif //  SENSOR_AM2322_H
