========================
sensorlib
========================

.. {# pkglts, doc

.. #}

Library of code to access sensors

Typical API


.. code::

    // current sensor status
    byte status();
    bool is_connected();

    // perform measurement.
    void sample(void);
    bool sample_not_ready(void);
    bool read_sample(void);
